package com.rezdy.api.model;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * Model Class for ingredient
 * 
 * @author pratulya.kashyap
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ingredient {

  @JsonProperty("title")
  private String title;

  @JsonProperty("best-before")
  private Date bestBefore;

  @JsonProperty("use-by")
  private Date useBy;

  /**
   * Constructor
   * 
   * @param title - title of the ingredient
   * @param bestBefore - bestBefore date
   * @param useBy - use by date
   */
  public Ingredient(String title, Date bestBefore, Date useBy) {
    this.title = title;
    this.bestBefore = bestBefore;
    this.useBy = useBy;
  }

  /**
   * Constructor
   * 
   * @param bestBefore - bestBefore date
   * @param useBy - use by date
   */
  public Ingredient(Date bestBefore, Date useBy) {
    this.bestBefore = bestBefore;
    this.useBy = useBy;
  }

  /**
   * Constructor
   * 
   * @param useBy - use by date
   */
  public Ingredient(Date useBy) {
    this.useBy = useBy;
  }

  /**
   * Constructor
   * 
   * @param title - title of the ingredient
   */
  public Ingredient(String title) {
    this.title = title;
  }

  /**
   * default constructor
   */
  public Ingredient() {

  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the bestBefore
   */
  public Date getBestBefore() {
    return bestBefore;
  }

  /**
   * @param bestBefore the bestBefore to set
   */
  public void setBestBefore(Date bestBefore) {
    this.bestBefore = bestBefore;
  }

  /**
   * @return the useBy
   */
  public Date getUseBy() {
    return this.useBy;
  }

  /**
   * @param useBy the useBy to set
   */
  public void setUseBy(Date useBy) {
    this.useBy = useBy;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Ingredient [title=");
    builder.append(title);
    builder.append(", bestBefore=");
    builder.append(bestBefore);
    builder.append(", useBy=");
    builder.append(useBy);
    builder.append("]");
    return builder.toString();
  }
}

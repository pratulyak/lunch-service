package com.rezdy.api.model;

import java.util.Collection;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model class for recipe
 * 
 * @author pratulya.kashyap
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Recipe {

  @JsonProperty("title")
  private String title;

  @JsonProperty("ingredients")
  private Collection<String> ingredients;

  /**
   * Generic constructor
   * 
   */
  public Recipe() {}

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the ingredients
   */
  public Collection<String> getIngredients() {
    return ingredients;
  }

  /**
   * @param ingredients the ingredients to set
   */
  public void setIngredients(List<String> ingredients) {
    this.ingredients = ingredients;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Recipe [title=");
    builder.append(title);
    builder.append(", ingredients=");
    builder.append(ingredients);
    builder.append("]");
    return builder.toString();
  }
}

package com.rezdy.api.controller;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.rezdy.api.model.Recipe;
import com.rezdy.api.service.RecipeService;

/**
 * @author pratulya.kashyap
 *
 */
@RestController
public class LunchPickerController {

  @Autowired
  private RecipeService recipeService;


  /**
   * API Endpoint /lunch which retrieves recipes based on condtions
   * 
   * @param useBy - flag to find all recipes bound by use-by date
   * @param bestBefore - flag to find all recipes bound by use-by and best-before date
   * 
   * @return {@link Collection} of recipes
   */
  @RequestMapping(method = RequestMethod.GET, value = "/lunch",
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public Collection<Recipe> getAvailableRecipes(
      @RequestParam(value = "useBy", required = false) Boolean useBy,
      @RequestParam(value = "bestBefore", required = false) Boolean bestBefore) {

    if (Boolean.TRUE.equals(useBy)) {
      return recipeService.getAllUseByRecipes();
    } else if (Boolean.TRUE.equals(bestBefore)) {
      return recipeService.getAvailableRecipes();
    }

    return recipeService.getAllRecipes();

  }

}

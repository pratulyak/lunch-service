package com.rezdy.api.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rezdy.api.model.Ingredient;
import com.rezdy.api.model.Recipe;

/**
 * @author pratulya.kashyap
 *
 */
@Service
public class ResourceService {

  private String INGREDIENTS = "https://www.mocky.io/v2/5cac82f1300000664f10368f";
  private String RECIPES = "https://www.mocky.io/v2/5c85f7a1340000e50f89bd6c";
  private String keystoreFile = "src/main/resources/my-release-key.keystore";
  private String keystorePassword = "rezdycom";
  private static final Logger log = LoggerFactory.getLogger(ResourceService.class);

  /**
   * Communicates with the mock end point to return list of ingredients
   * 
   * @return a list of ingredients
   * 
   */
  public List<Ingredient> getIngredients() {

    // uses restemplate to communicate
    ClientHttpRequestFactory factory = null;

    try {
      factory = getRequestFactory();
    } catch (KeyManagementException | UnrecoverableKeyException | KeyStoreException
        | NoSuchAlgorithmException | CertificateException | IOException e1) {
      log.error(e1.getMessage(), e1);
    }

    RestTemplate restTemplate = new RestTemplate(factory);
    String response = restTemplate.getForObject(INGREDIENTS, String.class);

    // parses the information
    ObjectMapper mapper = new ObjectMapper();
    Map<String, List<Ingredient>> ingredients = null;

    try {
      ingredients =
          mapper.readValue(response, new TypeReference<Map<String, List<Ingredient>>>() {});
    } catch (IOException e) {
      log.error(e.getMessage(), e);
    }

    // if key is null return default
    return ingredients.getOrDefault("ingredients", new ArrayList<Ingredient>());
  }

  /**
   * Communicates with the mock end point to return list of recipes
   * 
   * @return A List of recipes
   * 
   * @throws KeyStoreException - when there is an exception parsing the keystore files
   * @throws NoSuchAlgorithmException - when there is an exception setting ssl settings
   * @throws CertificateException - an exception when there is an error parsing the certificate
   * @throws FileNotFoundException - when the file is not found
   * @throws IOException - for IO issues
   * @throws KeyManagementException - exception while managing keys
   * @throws UnrecoverableKeyException - if key cannot be recovered
   */
  public List<Recipe> getRecipes() {

    // uses restemplate to communicate
    ClientHttpRequestFactory factory = null;

    try {
      factory = getRequestFactory();
    } catch (KeyManagementException | UnrecoverableKeyException | KeyStoreException
        | NoSuchAlgorithmException | CertificateException | IOException e1) {
      log.error(e1.getMessage(), e1);
    }

    RestTemplate restTemplate = new RestTemplate(factory);
    String response = restTemplate.getForObject(RECIPES, String.class);

    // parses the information
    ObjectMapper mapper = new ObjectMapper();
    Map<String, List<Recipe>> recipes = null;

    try {
      recipes = mapper.readValue(response, new TypeReference<Map<String, List<Recipe>>>() {});
    } catch (IOException e) {
      log.error(e.getMessage(), e);
    }

    // if key is null return default
    return recipes.getOrDefault("recipes", new ArrayList<Recipe>());
  }

  /**
   * This method sets up the required configurations and returns a {@link ClientHttpRequestFactory}
   * to be used to communnicate to the endpoints over SSL
   * 
   * 
   * @return {@link ClientHttpRequestFactory} - client request factory to be used with RestTemplate
   *         to communicate with the end points over SSL
   * 
   * @throws KeyStoreException - when there is an exception parsing the keystore files
   * @throws NoSuchAlgorithmException - when there is an exception setting ssl settings
   * @throws CertificateException - an exception when there is an error parsing the certificate
   * @throws FileNotFoundException - when the file is not found
   * @throws IOException - for IO issues
   * @throws KeyManagementException - exception while managing keys
   * @throws UnrecoverableKeyException - if key cannot be recovered
   */
  private ClientHttpRequestFactory getRequestFactory()
      throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException,
      FileNotFoundException, KeyManagementException, UnrecoverableKeyException {
    // Sets up required configurations to communicate through secure channel
    KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
    keyStore.load(new FileInputStream(new File(keystoreFile)), keystorePassword.toCharArray());

    // uses generated keystore information for ssl
    SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
        new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy())
            .loadKeyMaterial(keyStore, keystorePassword.toCharArray()).build(),
        NoopHostnameVerifier.INSTANCE);

    HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

    ClientHttpRequestFactory requestFactory =
        new HttpComponentsClientHttpRequestFactory(httpClient);
    return requestFactory;
  }
}

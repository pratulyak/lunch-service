package com.rezdy.api.service;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rezdy.api.model.Ingredient;
import com.rezdy.api.model.Recipe;

/**
 * A service that provides a list of available recipes based on ingredients
 * 
 * @author pratulya.kashyap
 *
 */
@Service
public class RecipeService {

  private static final Logger log = LoggerFactory.getLogger(RecipeService.class);

  @Autowired
  private ResourceService resourceService;

  /**
   * gets all recipes with ingredients before use by date
   * 
   * @return {@link LinkedHashSet} of recipes with insertion order
   */
  public LinkedHashSet<Recipe> getAllUseByRecipes() {

    Date today = new Date();

    List<Ingredient> allIngredients = null;
    List<Recipe> allRecipes = null;

    allIngredients = resourceService.getIngredients();
    allRecipes = resourceService.getRecipes();

    if (allIngredients == null || allRecipes == null) {
      log.error("The ingredients and/or recipes are not available.");
    }

    // retrieves all ingredients that are available before useBy
    Set<String> ingredientsUseBy =
        allIngredients.stream().filter(ingredient -> ingredient.getUseBy().after(today))
            .map(ingredient -> ingredient.getTitle()).collect(Collectors.toSet());

    // finds all recipes available in useby clause
    LinkedHashSet<Recipe> useByRecipes =
        allRecipes.stream().filter(recipe -> ingredientsUseBy.containsAll(recipe.getIngredients()))
            .collect(Collectors.toCollection(LinkedHashSet::new));

    return useByRecipes;
  }

  /**
   * This gets all recipes
   * 
   * @return returns a Collection of recipes
   */
  public Collection<Recipe> getAllRecipes() {
    return resourceService.getRecipes();
  }

  /**
   * This method returns a linkedHashset of recipes available based on ingredients available in the
   * 'fridge' with before before recipes at the end
   * 
   * @return {@link LinkedHashSet} a set that holds all recipes available
   */
  public LinkedHashSet<Recipe> getAvailableRecipes() {

    Date today = new Date();

    List<Ingredient> allIngredients = null;
    List<Recipe> allRecipes = null;

    allIngredients = resourceService.getIngredients();
    allRecipes = resourceService.getRecipes();

    if (allIngredients == null || allRecipes == null) {
      log.error("The ingredients and/or recipes are not available.");
    }

    // retrieves all ingredients available that are to be eaten last
    Set<String> ingredientsBestBefore = allIngredients.stream()
        .filter(ingredient -> ingredient.getBestBefore().before(today)
            && ingredient.getUseBy().after(today))
        .map(ingredient -> ingredient.getTitle()).collect(Collectors.toSet());

    // NOTE: Used linkedhashset here so that insertion order is maintained for
    // sorting and no duplicates

    // finds all recipes available in useby clause
    LinkedHashSet<Recipe> useByRecipes = this.getAllUseByRecipes();
    // finds all recipes that need to be consumed last fitting best-before criteria
    LinkedHashSet<Recipe> bestBeforeRecipes = allRecipes.stream()
        .filter(recipe -> ingredientsBestBefore.containsAll(recipe.getIngredients()))
        .collect(Collectors.toCollection(LinkedHashSet::new));

    // should have insertion order maintained to have second clause recipes if exist
    // add to final set
    useByRecipes.addAll(bestBeforeRecipes);

    return useByRecipes;
  }

}

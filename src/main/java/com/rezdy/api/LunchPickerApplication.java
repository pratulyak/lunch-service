package com.rezdy.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author pratulya.kashyap
 *
 */
@SpringBootApplication
public class LunchPickerApplication {

  /**
   * Main application runner
   * 
   * @param args arguments to pass through to the main method
   */
  public static void main(String[] args) {
    SpringApplication.run(LunchPickerApplication.class, args);
  }
}

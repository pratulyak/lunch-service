package com.rezdy.api.service;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;
import com.rezdy.api.model.Ingredient;
import com.rezdy.api.model.Recipe;

/**
 * @author pratulya.kashyap
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestResourceService {

  @Autowired
  private ResourceService resourceService;


  @Test
  public void testResourceService() {

    Assert.notNull(resourceService, "resourceService is null");
  }

  /**
   * Tests that the ingredients list is not empty and the first and last ingredients are Ham and
   * Salad dressing respectively
   */
  @Test
  public void testGetIngredients() {

    List<Ingredient> ingredients = resourceService.getIngredients();

    Assert.notEmpty(ingredients, "List of ingredients is empty");
    Assert.isTrue(ingredients.size() == 16, "Ingredients list is not 16 it's" + ingredients.size());

    Ingredient testIngredient = ingredients.get(0);

    Assert.isTrue(testIngredient.getTitle().equalsIgnoreCase("Ham"), "First ingredient is not Ham");
    Assert.notNull(testIngredient.getBestBefore(), "First ingredients best before is null");
    Assert.notNull(testIngredient.getUseBy(), "First ingredients use by is null");


    Ingredient testIngredient2 = ingredients.get(ingredients.size() - 1);

    Assert.isTrue(testIngredient2.getTitle().equalsIgnoreCase("Salad Dressing"),
        "Last ingredient is not Salad dressing");
    Assert.notNull(testIngredient2.getBestBefore(), "Last ingredient's best before is null");
    Assert.notNull(testIngredient2.getUseBy(), "last ingredient's use by is null");
  }

  /**
   * Tests that the recipes list is not empty and first ingredient is Ham and Cheese Toastie and has
   * the ingredients ham cheese bread and butter
   * 
   */
  @Test
  public void testGetRecipes() {

    List<Recipe> recipes = resourceService.getRecipes();


    Assert.notEmpty(recipes, "List of recipes is empty");
    Assert.isTrue(recipes.size() == 5, "recipes list is not 5 it is " + recipes.size());

    Recipe testRecipe = recipes.get(0);


    Assert.isTrue(testRecipe.getTitle().equalsIgnoreCase("Ham and Cheese Toastie"),
        "First ingredient is not Ham and Cheese Toastie it is " + testRecipe.getTitle());
    Assert.isTrue(
        testRecipe.getIngredients().containsAll(Arrays.asList("Ham", "Cheese", "Bread", "Butter")),
        "Some or all ingredients are not present");
  }
}

package com.rezdy.api.service;

import java.util.LinkedHashSet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;
import com.rezdy.api.model.Recipe;

/**
 * @author pratulya.kashyap
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestRecipeService {

  @Autowired
  private RecipeService recipeService;

  @Test
  public void testRecipeService() {
    Assert.notNull(recipeService, "recipeService is null");
  }

  /**
   * test if recipes are returned
   */
  @Test
  public void testGetAvailableRecipes() {
    LinkedHashSet<Recipe> recipes = recipeService.getAvailableRecipes();
    Assert.notNull(recipes, "no recipes return through recipeService");
  }
}

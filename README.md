#Rezdy Technical Task

##Background:
This API provides avaiable recipes from a list of recipes and ingredients. It communicates with end points to receive the list and filters the recipes on the dates of use of the ingredients

##Requirements:
Maven
JDK 1.8+
port number 4578


##Instructions:
###Pre Conditions: JDK 1.8+ and port 4578 is available
1. Use Maven to build the project to output as a jar
2. Go to the directory with the jar and execute:

`````````
java -jar target/lunch-picker-api-0.0.1-SNAPSHOT.jar
`````````


##Usage:

This will retrieve all the recipes available. 

```
localhost:4578/lunch
```

###Params:
* useBy: This retrieves all recipes with ingredients bound by the use-by date

```
localhost:4578/lunch?useBy=true
```
* bestBefore: This retrieves all recipes with ingredients bound by use-by date and are sorted with best before expired ingredient recipes at the end of the list

```
localhost:4578/lunch?bestBefore=true
```

